Technical specifics
- Dependencies `google-protobuf`, `grpc`
- Dev dependencies `grpc-tools` - used to generate TS "glue" code

Setup
- app.ts creates a grpc server, this requires a handler which overrides the rpc defined in the proto
- `.proto` files are currently stored in `src/proto/<handler_name>/<handler>.proto`, however they can be stored anywhere.
- The `build_proto.sh` file generates the TS interface code and stores these in `bin/generated` using the same fs.
- The eslint file treats `bin/generated` as a rootDir so generated protos can be referenced as `proto/<handler>/<hander>_pb.ts`)

Development
- Setup a file watcher so the npm task `build:proto` is ran on .proto file changes.

Things to consider
- These protos will need to be shared between all services that want to interact with it. 
  Therefore a common folder structure should be formulated.
- Authentication can either be done using private/public keys. Or the grpc servers could be kept in a restrained pool of
ports and outside traffic can be denied (preferred). No external service should have to communicate with the RPC for any
reason.
- In actual releases the `bin` folder should be excluded.