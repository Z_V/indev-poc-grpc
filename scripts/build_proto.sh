#!/usr/bin/env bash

BASEDIR=$(dirname "$0")
# shellcheck disable=SC2164
cd "${BASEDIR}"/../

PROTOC_GEN_TS_PATH="./node_modules/.bin/protoc-gen-ts"
GRPC_TOOLS_NODE_PROTOC_PLUGIN="./node_modules/.bin/grpc_tools_node_protoc_plugin"
GRPC_TOOLS_NODE_PROTOC="./node_modules/.bin/grpc_tools_node_protoc"

for f in ./src/proto/*; do

  if [ "$(basename "$f")" == "index.ts" ]; then
      continue
  fi

  generatedDir="./bin/generated/${f}"
  transformedDir="${generatedDir/.\/src\/}"

  echo "Building: ${f}. Creating generated files at: ${transformedDir}"
  mkdir -p "${transformedDir}"

  ${GRPC_TOOLS_NODE_PROTOC} \
      --js_out=import_style=commonjs,binary:"${transformedDir}" \
      --grpc_out="${transformedDir}" \
      --plugin=protoc-gen-grpc="${GRPC_TOOLS_NODE_PROTOC_PLUGIN}" \
      -I "${f}" \
      "${f}"/*.proto

  ${GRPC_TOOLS_NODE_PROTOC} \
      --plugin=protoc-gen-ts="${PROTOC_GEN_TS_PATH}" \
      --ts_out="${transformedDir}" \
      -I "${f}" \
      "${f}"/*.proto
done
