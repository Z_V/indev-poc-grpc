import * as grpc from 'grpc';
import {IRobloxServer, RobloxService} from "proto/roblox/roblox_grpc_pb";
import {AssetRequest, AssetResponse, AssetType} from "proto/roblox/roblox_pb";


class UploadAssetHandler implements IRobloxServer {
  uploadAsset = (call: grpc.ServerUnaryCall<AssetRequest>, callback: grpc.sendUnaryData<AssetResponse>): void => {
    const reply: AssetResponse = new AssetResponse();

    if (call.request.getType() === AssetType.MODULE){
      reply
        .setSuccess(true)
        .setId(3)
    } else {
      reply
        .setSuccess(false)
        .setId(-1)
    }

    callback(null, reply);
  };
}

export default {
  service: RobloxService,
  handler: new UploadAssetHandler(),
};
