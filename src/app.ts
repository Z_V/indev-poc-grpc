import * as grpc from "grpc";
import UploadAssetHandler from "handlers/UploadAssetHandler";

const server: grpc.Server = new grpc.Server();

server.addService(UploadAssetHandler.service, UploadAssetHandler.handler);

server.bindAsync(
  `0.0.0.0:6000`,
  grpc.ServerCredentials.createInsecure(),
  (err: Error, port: number) => {
    if (err != null) {
      return console.error(err);
    }
    console.log(`gRPC listening on ${ port }`);
  },
);

server.start();
