// package: roblox
// file: roblox.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class AssetRequest extends jspb.Message { 
    getTitle(): string;
    setTitle(value: string): AssetRequest;

    getContents(): string;
    setContents(value: string): AssetRequest;

    getSandboxed(): boolean;
    setSandboxed(value: boolean): AssetRequest;

    getType(): AssetType;
    setType(value: AssetType): AssetRequest;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): AssetRequest.AsObject;
    static toObject(includeInstance: boolean, msg: AssetRequest): AssetRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: AssetRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): AssetRequest;
    static deserializeBinaryFromReader(message: AssetRequest, reader: jspb.BinaryReader): AssetRequest;
}

export namespace AssetRequest {
    export type AsObject = {
        title: string,
        contents: string,
        sandboxed: boolean,
        type: AssetType,
    }
}

export class AssetResponse extends jspb.Message { 
    getId(): number;
    setId(value: number): AssetResponse;

    getSuccess(): boolean;
    setSuccess(value: boolean): AssetResponse;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): AssetResponse.AsObject;
    static toObject(includeInstance: boolean, msg: AssetResponse): AssetResponse.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: AssetResponse, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): AssetResponse;
    static deserializeBinaryFromReader(message: AssetResponse, reader: jspb.BinaryReader): AssetResponse;
}

export namespace AssetResponse {
    export type AsObject = {
        id: number,
        success: boolean,
    }
}

export enum AssetType {
    LOCAL = 0,
    SERVER = 1,
    MODULE = 2,
    RAW = 3,
}
