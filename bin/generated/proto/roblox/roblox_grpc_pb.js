// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var roblox_pb = require('./roblox_pb.js');

function serialize_roblox_AssetRequest(arg) {
  if (!(arg instanceof roblox_pb.AssetRequest)) {
    throw new Error('Expected argument of type roblox.AssetRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_roblox_AssetRequest(buffer_arg) {
  return roblox_pb.AssetRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_roblox_AssetResponse(arg) {
  if (!(arg instanceof roblox_pb.AssetResponse)) {
    throw new Error('Expected argument of type roblox.AssetResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_roblox_AssetResponse(buffer_arg) {
  return roblox_pb.AssetResponse.deserializeBinary(new Uint8Array(buffer_arg));
}


var RobloxService = exports.RobloxService = {
  uploadAsset: {
    path: '/roblox.Roblox/uploadAsset',
    requestStream: false,
    responseStream: false,
    requestType: roblox_pb.AssetRequest,
    responseType: roblox_pb.AssetResponse,
    requestSerialize: serialize_roblox_AssetRequest,
    requestDeserialize: deserialize_roblox_AssetRequest,
    responseSerialize: serialize_roblox_AssetResponse,
    responseDeserialize: deserialize_roblox_AssetResponse,
  },
};

exports.RobloxClient = grpc.makeGenericClientConstructor(RobloxService);
