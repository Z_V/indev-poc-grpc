// package: roblox
// file: roblox.proto

/* tslint:disable */
/* eslint-disable */

import * as grpc from "grpc";
import * as roblox_pb from "./roblox_pb";

interface IRobloxService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
    uploadAsset: IRobloxService_IuploadAsset;
}

interface IRobloxService_IuploadAsset extends grpc.MethodDefinition<roblox_pb.AssetRequest, roblox_pb.AssetResponse> {
    path: "/roblox.Roblox/uploadAsset";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<roblox_pb.AssetRequest>;
    requestDeserialize: grpc.deserialize<roblox_pb.AssetRequest>;
    responseSerialize: grpc.serialize<roblox_pb.AssetResponse>;
    responseDeserialize: grpc.deserialize<roblox_pb.AssetResponse>;
}

export const RobloxService: IRobloxService;

export interface IRobloxServer {
    uploadAsset: grpc.handleUnaryCall<roblox_pb.AssetRequest, roblox_pb.AssetResponse>;
}

export interface IRobloxClient {
    uploadAsset(request: roblox_pb.AssetRequest, callback: (error: grpc.ServiceError | null, response: roblox_pb.AssetResponse) => void): grpc.ClientUnaryCall;
    uploadAsset(request: roblox_pb.AssetRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: roblox_pb.AssetResponse) => void): grpc.ClientUnaryCall;
    uploadAsset(request: roblox_pb.AssetRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: roblox_pb.AssetResponse) => void): grpc.ClientUnaryCall;
}

export class RobloxClient extends grpc.Client implements IRobloxClient {
    constructor(address: string, credentials: grpc.ChannelCredentials, options?: object);
    public uploadAsset(request: roblox_pb.AssetRequest, callback: (error: grpc.ServiceError | null, response: roblox_pb.AssetResponse) => void): grpc.ClientUnaryCall;
    public uploadAsset(request: roblox_pb.AssetRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: roblox_pb.AssetResponse) => void): grpc.ClientUnaryCall;
    public uploadAsset(request: roblox_pb.AssetRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: roblox_pb.AssetResponse) => void): grpc.ClientUnaryCall;
}
